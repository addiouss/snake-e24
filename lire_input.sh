lire_input() {
    if read -n 1 -s -t 0.1 key; then
        case "$key" in
            d) if [[ $direction != "q" ]]; then direction="d"; fi ;;  # Droite
            q) if [[ $direction != "d" ]]; then direction="q"; fi ;;  # Gauche
            z) if [[ $direction != "s" ]]; then direction="z"; fi ;;  # Haut
            s) if [[ $direction != "z" ]]; then direction="s"; fi ;;  # Bas
            *) echo mauvaise touche
        esac
    fi
}
