#!/bin/bash
source ./menu.sh
source ./boucle_du_jeu.sh
source ./deplacement.sh
source ./afficher_plateau.sh
source ./generer_pommes.sh
source ./lire_input.sh
source ./affichage_scores.sh

plateau_largeur=10
plateau_longueur=10
snake_x=5
snake_y=5
longueur_snake=1
direction="d"
snake_body=()
pomme_x=0
pomme_y=0
etat_jeu=1

main() {
    while true; do
	show_menu
        case "$option" in
            1)
                generer_pommes
		etat_jeu=1
		boucle_du_jeu
		afficher_score
		snake_x=5
		snake_y=5
		snake_body=()
		direction="d"
		longueur_snake=1
                ;;
            2)
                echo "Merci d'avoir joué!"
                exit 0
                ;;
            *)
                echo "Option invalide. Veuillez réessayer."
                show_menu
                ;;
        esac
    done
}

main
