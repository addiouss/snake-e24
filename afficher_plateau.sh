
#!/usr/bin/env bash
# $1 est une liste des coo de pomme, $2 est une liste du corps du snake, $3 est un entier de la longueur du plateau
function afficher_plateau { 
clear
for y in $(seq 0 "$plateau_longueur")
do
	for x in $(seq 0 "$plateau_longueur")
	do
		if [ "$x" -eq 0 ] || [ "$y" -eq 0 ] || [ "$x" -eq "$plateau_longueur" ] || [ "$y" -eq "$plateau_longueur" ]
		then
			echo -n " ⬛";
		elif [[ $x -eq $snake_x && $y -eq $snake_y ]]; then
			echo -n " ⬛"
		elif test "$x" -eq "$pomme_x" && test "$y" -eq "$pomme_y"
		then
			echo -n " 🟥";
		else
			body_part=0
			for part in "${snake_body[@]}"; do
				IFS="," read part_x part_y <<< "$part"
				if [[ "$x" -eq "$part_x" && "$y" -eq "$part_y" ]]; then
					echo -n " 🟩"
					body_part=1
					break
				fi
			done
			if [[ "$body_part" -eq 0 ]]; then
				echo -n " ⬜"
			fi
		fi
	done
	echo ""
done
}
