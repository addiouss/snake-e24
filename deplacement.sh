position_serpent() {
    snake_body=("$snake_x,$snake_y" "${snake_body[@]}")
    if [[ ${#snake_body[@]} -gt $longueur_snake ]]; then
        snake_body=("${snake_body[@]:0:$longueur_snake}")
    fi
    case "$direction" in
        d) ((snake_x++)) ;;
        q) ((snake_x--)) ;;
        z) ((snake_y--)) ;;
        s) ((snake_y++)) ;;
    esac
    if [[ $snake_x -eq $pomme_x && $snake_y -eq $pomme_y ]]; then
        ((longueur_snake++))
        generer_pommes
    fi

    if [[ $snake_x -le 0 || $snake_x -ge $plateau_longueur || $snake_y -le 0 || $snake_y -ge $plateau_longueur ]]
	then
        etat_jeu=0
    fi
    for part in "${snake_body[@]}"; do
        IFS=',' read part_x part_y <<< "$part"
        if [[ $snake_x -eq $part_x && $snake_y -eq $part_y ]]; then
            etat_jeu=0
        fi
    done
}
