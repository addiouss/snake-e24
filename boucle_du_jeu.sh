#Fonction qui gère la boucle du jeu 

function boucle_du_jeu {

	while [[ $etat_jeu -eq 1 ]]; do
    		afficher_plateau
    		lire_input
    		position_serpent
		sleep 0.25
	done

	echo -n "Game Over! Votre score est: "
}

